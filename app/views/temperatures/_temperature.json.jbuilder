json.extract! temperature, :id, :sensor_id, :temp, :day, :at, :created_at, :updated_at
json.url temperature_url(temperature, format: :json)
