class Sensor < ApplicationRecord
  has_many :temperatures
  validates :device, presence: true
end
