class Temperature < ApplicationRecord
  belongs_to :sensor
  validates :sensor_id, presence: true
end
