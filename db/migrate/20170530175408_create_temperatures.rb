class CreateTemperatures < ActiveRecord::Migration[5.1]
  def change
    create_table :temperatures do |t|
      t.integer :sensor_id
      t.float :temp
      t.date :day
      t.time :at

      t.timestamps
    end
  end
end
